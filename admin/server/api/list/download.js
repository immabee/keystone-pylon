var keystone = require('../../../../');
var moment = require('moment');
var assign = require('object-assign');
var nodeExcel = require('excel-export');
var _ = require('lodash');

module.exports = function (req, res) {
	var baby = require('babyparse');
	var format = req.params.format.split('.')[1]; // json or csv
	var where = {};
	var filters = req.query.filters;
	if (filters && typeof filters === 'string') {
		try { filters = JSON.parse(req.query.filters); }
		catch (e) { } // eslint-disable-line no-empty
	}
	if (typeof filters === 'object') {
		assign(where, req.list.addFiltersToQuery(filters));
	}
	if (req.query.search) {
		assign(where, req.list.addSearchToQuery(req.query.search));
	}
	var query = req.list.model.find(where);
	if (req.query.populate) {
		query.populate(req.query.populate);
	}
	if (req.query.expandRelationshipFields) {
		req.list.relationshipFields.forEach(function (i) {
			query.populate(i.path);
		});
	}
	var sort = req.list.expandSort(req.query.sort);
	query.sort(sort.string);
	query.exec(function (err, results) {
		var data;
		if (err) return res.apiError('database error', err);
		var docs = [];

		if (req.list.model.modelName === 'AnalysisResult') {

			results.forEach(function (result) {
				//console.log(item);
				if (!_.isEmpty(result.result.analysis.results)) {
					var doc = {
						date: result.start,
						result: result.result.analysis.results
					}
					docs.push(doc)
				}
			});
			var excelConf = {};
			excelConf.cols = [
				{
					caption: 'Date'
				}
			];
			excelConf.rows = [];
			if (docs.length) {
				if (results[0].query.name && (results[0].query.name.indexOf('timeSeries') > -1)) {
					for (var i = 0; i < 24; i++) {
						excelConf.cols.push({ caption: i + 'h', type: 'number' });
					}

					docs.forEach(function (doc) {
			            if (doc) {
			                var record = [
								moment(doc.date).format('DD-MM-YYYY')
			                ];
			                excelConf.cols.forEach(function(col) {
								doc.result.forEach(function(item) {
									if (moment(parseInt(item.key) * 1000).hour() == parseInt(col.caption.replace('h'))) {
										record.push(item.interactions);
									}
								})
			                })

			                excelConf.rows.push(record);
			            }
			        });
				} else {
					docs[0].result.forEach(function(item) {
						excelConf.cols.push({ caption: item.key, type: 'number' })
					})

					docs.forEach(function (doc) {
			            if (doc) {
			                var record = [
								moment(doc.date).format('DD-MM-YYYY')
			                ];
			                excelConf.cols.forEach(function(col) {
								doc.result.forEach(function(item) {
									if (item.key == col.caption) {
										record.push(item.interactions)
									}
								})
			                })

			                excelConf.rows.push(record);
			            }
			        });
				}
			}
			var finalResult = nodeExcel.execute(excelConf);
			res.setHeader('Content-Type', 'application/vnd.openxmlformats');
			res.setHeader('Content-Disposition', 'attachment; filename=' + results[0].query.name + moment().format('YYYYMMDD-HHmmss') + '.xlsx');
			res.end(finalResult, 'binary');
		} else {
			if (format === 'csv') {
				data = results.map(function (item) {
					//console.log(item);
					return req.list.getCSV(item, req.query.select, req.query.expandRelationshipFields);
				});
				res.attachment(req.list.path + '-' + moment().format('YYYYMMDD-HHmmss') + '.csv');
				res.setHeader('Content-Type', 'application/octet-stream');
				var content = baby.unparse(data, {
					delimiter: keystone.get('csv field delimiter') || ',',
				});
				res.end("\ufeff" + content, 'utf-8');
			} else {
				data = results.map(function (item) {
					return req.list.getCSV(item, req.query.select, req.query.expandRelationshipFields);
				});
				res.json(data);
			}
		}	
	});
};
